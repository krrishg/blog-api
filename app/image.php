<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;

class image extends Model
{
    protected $fillable = ['images','post_id'];

    public function post() {
        return $this->belongsTo(Post::class);
    }

}
