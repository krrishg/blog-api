<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Comment;
use App\image;
class Post extends Model
{

    protected $fillable = ['title', 'body', 'user_id'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function posts(){
        return $this->hasMany(Post::class);
    }
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function images(){
        return $this->hasMany(image::class);
    }
    
}
