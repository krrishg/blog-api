<?php

namespace App\Http\Controllers;
use App\Http\Requests\postCreateRequest;
use App\Http\Requests\postUpdateRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\PostResource as PostResource;
use App\Post;
use App\image;
//use Illuminate\Http\Request;

class PostController extends Controller
{
    public function store(postCreateRequest $request){
        $post = new Post;
        $post->title = $request->title;
        $post->body = $request->body;
        $post->publish = $request->publish;
        $post->user()->associate($request->user());

        if($request->hasFile('images')){
            $filenameWithExt = $request->file('images')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('images')->getClientOriginalExtension();
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            $path = $request->file('images')->storeAs('public/images/images', $fileNameToStore);
        } else {
                $fileNameToStore = 'noimage.jpg';
            }
                // Create Post
                $image = new image;
                $image->images = $fileNameToStore;
                $image->post_id = $request->post_id;
                $post->save();
                $post->images()->save($image);
                return new PostResource($post);
    }

    public function update(postUpdateRequest $request ,Post $post){
        $this->authorize('update', $post);
        $post->title = $request->get('title',$post->title);
        $post->body = $request->get('body',$post->body);
        $post->publish = $request->get('publish',$post->publish);
        $post->save();
        return new PostResource($post);
    }

    public function getIndividualPost(Post $post){  
        $posts = Post::where('user_id', '=', Auth::user()->id)->get();
        return PostResource::collection($posts);     
    }

    public function show(Post $post){
        return new PostResource($post);
    }

    public function index(){
        $posts = Post::where('publish', '=','public')->get();
        return PostResource::collection($posts);
    }

    public function getPrivatePost(Post $post){
        $posts = Post::where('publish', '=','private')
        ->where('user_id', '=', Auth::user()->id)
        ->get();
        return PostResource::collection($posts);
    }

    public function destroy(Post $post){
        $this->authorize('destroy', $post);
        $post->delete();
        return response(null, 204);
    }
}
