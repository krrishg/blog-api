<?php

namespace App\Http\Controllers;
use App\User;
use App\Http\Requests\UserRegisterRequest;
use App\Http\Requests\UserLoginRequest;
use Illuminate\Http\Request;
use App\Http\Resources\User as UserResource;

class AuthController extends Controller
{
    public function register(UserRegisterRequest $request){
        if($request->hasFile('profile_image')){
            $filenameWithExt = $request->file('profile_image')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('profile_image')->getClientOriginalExtension();
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            $path = $request->file('profile_image')->storeAs('public/images/images', $fileNameToStore);
        } else {
                $fileNameToStore = 'noimage.jpg';
            }
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->profile_image = $fileNameToStore;
            $user->password = bcrypt($request->password);
            $user->save();

        if (!$token = auth()->attempt($request->only(['email','password']))) {
            return abort(401);
        }

        return ( new UserResource($request ->user())) -> additional([
            'meta' => [
                'token' =>$token,
            ]
        ]);
    }

    public function login(UserLoginRequest  $request){
        if (!$token = auth()->attempt($request->only(['email','password']))) {
            return response()->json([
                'errors'=>[
                    'email' => ['sorry!! we cannot find you with those details'],
                ]
                ], 422);
        }

        return ( new UserResource($request ->user())) -> additional([
            'meta' => [
                'token' =>$token,
            ]
        ]);
    }

    public function user(Request $request){
        return new UserResource($request->user());
    }

    public function logout(){
        auth()->logout();
    }
}
