<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Post;
use App\Http\Resources\CommentResource as CommentResource;
use App\Http\Requests\storeCommentRequest;

class CommentController extends Controller
{
    public function store(Post $post, storeCommentRequest $request)
    {
        $comment = $post->comments()->create([
            'body' => $request->body,
            'user_id' => $request->user()->id
        ]);
        return new CommentResource($comment->load(['post', 'user', 'post.user']));
    }
}
